#***************************************************************************
#* SPDX-FileCopyrightText: 2024 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2024 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE GRANTLEE_FILTERS ::..")

PROJECT(grantlee_filters)

IF(QT_MAJOR_VERSION STREQUAL "5")
    FIND_PACKAGE(Grantlee5 0.5 REQUIRED)
ENDIF()
ADD_LIBRARY(grantlee_skgfilters MODULE
    skggrantleefilters.cpp
    skgdocumentfilter.cpp
    skgobjectfilter.cpp)
SET_PROPERTY(TARGET grantlee_skgfilters PROPERTY EXPORT_NAME skggrantleefilters)
TARGET_LINK_LIBRARIES(grantlee_skgfilters KF${QT_MAJOR_VERSION}::CoreAddons Qt${QT_MAJOR_VERSION}::Core skgbasemodeler skgbasegui)
IF(QT_MAJOR_VERSION STREQUAL "5")
    GRANTLEE_ADJUST_PLUGIN_NAME(grantlee_skgfilters)
else()
    TARGET_LINK_LIBRARIES(grantlee_skgfilters KF6::TextTemplate)
endif()

########### install files ###############

IF(QT_MAJOR_VERSION STREQUAL "5")
INSTALL(TARGETS grantlee_skgfilters LIBRARY DESTINATION ${KDE_INSTALL_QTPLUGINDIR}/grantlee/${Grantlee5_VERSION_MAJOR}.${Grantlee5_VERSION_MINOR} )
else()
INSTALL(TARGETS grantlee_skgfilters LIBRARY DESTINATION ${KDE_INSTALL_QTPLUGINDIR}/kf6/ktexttemplate )
endif()
